import 'package:polymer/builder.dart';

void main(List<String> args) {
  lint(entryPoints: ['example/web/demo.html'], options: parseOptions(args));
}
